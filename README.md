# RMM Laravel Boilerplate 

Boilerplate pessoal para criação de aplicações, pocs e estudo da tecnologia.

- Laravel 8.7.x

# Requisitos

- Php >= 7.4
- Composer > 2.x 
## Instalação

Para clonar o projeto

```bash
$ git clone git@gitlab.com:ricardo-melo-martins/rmm-laravel-boilerplate.git
```

Instale os pacotes com o comando

```bash
$ composer install
```

Para executar o projeto

```bash
$ php artisan serve
```

Abra o navegador e digite a url
http://127.0.0.1:8000

## Licença

O RMM Laravel Boilerplate e o Framework Laravel são de código aberto licenciado sob a [licença MIT](https://opensource.org/licenses/MIT).